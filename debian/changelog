madlib (1.3.0-4) unstable; urgency=medium

  * QA upload.

  [ Vagrant Cascadian ]
  * debian/control: Add Vcs-* Headers.

  [ Petter Reinholdtsen ]
  * Updated Standards-Version from 3.8.3 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Flag madlib-doc with Multi-Arch: foreign.
  * Added d/gbp.conf documenting git branch with debian build rules.

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 30 May 2024 21:59:36 +0200

madlib (1.3.0-3) unstable; urgency=medium

  * QA upload.

  [ Vagrant Cascadian ]
  * debian/control: Set "Debian QA Group" as Maintainer.
  * Switch to source-format 3.0 (quilt). (Closes: #1007462)
  * Switch to dh and debhelper-compat 10.
  * debian/rules: Remove build path in dh_installexamples override.
  * debian/control: Drop libmadlib-dbg.
  * debian/control: Change to Priority "optional".
  * debian/control: Update Homepage URL.
  * debian/control: Remove obsolete Vcs-* headers.
  * debian/rules: Drop get-orig-source target.
  * debian/control: Set Rules-Requires-Root to "no".
  * debian/watch: Update for new location.

  [ Chris Lamb ]
  * debian/rules: Use --with-hostname to set hostname (Closes: #778946)

 -- Vagrant Cascadian <vagrant@reproducible-builds.org>  Fri, 21 Oct 2022 16:08:44 -0700

madlib (1.3.0-2.2) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 13:42:59 +0100

madlib (1.3.0-2.1) unstable; urgency=low

  * Non maintainer upload.
  * Fix build failure with GCC 4.7. Closes: #667267.

 -- Matthias Klose <doko@debian.org>  Tue, 17 Apr 2012 08:37:01 +0200

madlib (1.3.0-2) unstable; urgency=low

  [Christophe Prud'homme]
  * Bug fix: "FTBFS with GCC 4.4: missing #include", thanks to Martin
    Michlmayr (Closes: #553702).
  * Bug fix: "long description no sentence", thanks to Gerfried Fuchs
    (Closes: #553245).

 -- Christophe Prud'homme <prudhomm@debian.org>  Sun, 20 Dec 2009 20:25:07 +0100

madlib (1.3.0-1) unstable; urgency=low

  [ Christophe Prud'homme ]
  * New upstream release

 -- Christophe Prud'homme <prudhomm@debian.org>  Sat, 17 Oct 2009 20:53:16 +0200

madlib (1.2.3-1) unstable; urgency=low

  [ Christophe Prud'homme ]
  * Initial release (Closes: #539861))

 -- Christophe Prud'homme <prudhomm@debian.org>  Sun, 11 Oct 2009 16:25:53 +0200
